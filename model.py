import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.integrate import odeint
from functions import Ecosystem_ode


#opc_light = True
opc_light = True

# Configuration
time_0 = 0
time_max = 86400*30*1
time_dt = 1
t_series = np.arange(time_0,time_max+1,time_dt)

# Initial conditions
"""phy_0 = 0.2
ip_0 = 0.004
op_0 = 0.01"""

""" Stable Solution"""
phy_0 = 20#5590.01785655#5590.01785247#5590.0619701#20#0.1
ip_0 = 500#38.86091636#38.86091631#38.86197465#500#0.00216
op_0 = 452#773.88863723#773.88863738#773.8864761#452#0.009625
#
em_0 = 0#63.72436237#63.7243624#63.72327596
#

# Temperature
T_w = 20

# Light configuration from .lig file
light = 'incident.lig'
I       = 180/24/3600
t_U     = 6
t_D     = 18
if opc_light:
    light_data = np.loadtxt(light)


# ode integration with scipy
r = integrate.ode(Ecosystem_ode).set_integrator("lsoda")
r.set_initial_value([phy_0,ip_0,op_0,em_0],time_0).set_f_params(T_w,I,t_U,t_D)

step_num = len(t_series)

t_ode = np.zeros([step_num,1])
phy_ode = np.zeros([step_num,1])
ip_ode = np.zeros([step_num,1])
op_ode = np.zeros([step_num,1])
em_ode = np.zeros([step_num,1])

t_ode[0] = time_0
phy_ode[0] = phy_0
ip_ode[0] = ip_0
op_ode[0] = op_0
em_ode[0] = em_0

ite = 1
print(""""
********* Start Compute Numerical Solution *********
""")
while r.successful() and ite<step_num:
    if opc_light:
        if (light_data[0,0] <= r.t and r.t < light_data[1,0]):
            I = light_data[0,1]/24/3600
            t_U = light_data[0,2]
            t_D = light_data[0,3]
        elif (light_data[1,0] <= r.t and r.t < light_data[2,0]):
            I = light_data[1,1]/24/3600
            t_U = light_data[1,2]
            t_D = light_data[1,3]
        elif(light_data[2,0] <= r.t):
            I = light_data[-1,1]/24/3600
            t_U = light_data[-1,2]
            t_D = light_data[-1,3]

        r.set_f_params(T_w,I,t_U,t_D)
    r.integrate(r.t + time_dt)
    t_ode[ite] = r.t
    phy_ode[ite] = r.y[0]
    ip_ode[ite] = r.y[1]
    op_ode[ite] = r.y[2]
    em_ode[ite] = r.y[3]
    ite += 1


print("""
********* Enzyme plot *********
""")
fig = plt.figure("Enzyme process")
initial = 86400 * 15 // time_dt
final = None
step = 86400 // time_dt // 24

#plt.rc('font', family='serif')

ax = fig.add_subplot(411)
ax.plot(t_ode[initial:final:step],phy_ode[initial:final:step],'gx', label='ode phy')
ax.set_xticks([])
ax.set_ylabel(r'$(mg C\cdot L^{-1})$')
plt.legend()

ax = fig.add_subplot(412)
ax.plot(t_ode[initial:final:step],ip_ode[initial:final:step],'rx', label='ode ip')
ax.set_xticks([])
ax.set_ylabel(r'$(mg P\cdot L^{-1})$')
plt.legend()

ax = fig.add_subplot(413)
ax.plot(t_ode[initial:final:step],op_ode[initial:final:step],'bx', label='ode op')
ax.set_xticks([])
ax.set_ylabel(r'$(mg P\cdot L^{-1})$')
plt.legend()

ax = fig.add_subplot(414)
ax.plot(t_ode[initial:final:step],em_ode[initial:final:step],'x', label='ode em')
ax.set_xlabel('time (days)')
ax.set_ylabel(r'$(mg P\cdot L^{-1})$')

step_label = 86400*15//time_dt
seg2day = t_ode[initial:final:step_label,0] // 86400
#print(t_ode[initial:final:step_label,0])
ax.set_xticks(t_ode[initial:final:step_label,0])
ax.set_xticklabels(seg2day)

plt.legend()


print(phy_ode[-1], ip_ode[-1], op_ode[-1], em_ode[-1])

plt.show()
