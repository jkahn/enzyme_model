import numpy as np
import datetime as d
from scipy import optimize
from scipy.interpolate import interp1d
#from astral import Astral

import parametres as p

def G_p(C_ip,C_p,T_w,t,t_U,t_D,I):
    f1      = C_ip/(p.K_mP+C_ip)
    return p.k_gr*(p.temp_gr**(T_w-20))*f1

def D_p(T_w):
    return p.k_r2*(p.temp_r2**(T_w-20))+(p.k_par+p.k_grz)*(p.temp_mr**(T_w-20))

def Ecosystem_ode(t,x,T_w,I,t_U,t_D):
    C_p     = x[0]
    C_ip    = x[1]
    C_op    = x[2]
    C_em    = x[3]

    S_p     = 0
    S_ip    = 0
    S_op    = 0
    S_em    = 0

    # phytoplankton growth
    phyto_growth 	     = G_p(C_ip,C_p,T_w,t,t_U,t_D,I) * C_p

    S_p                  += phyto_growth
    S_ip                 -= p.a_pc * phyto_growth


    # death and respiration of the phytoplankton
    phyto_death         = D_p(T_w) * C_p

    S_p                 -= phyto_death
    S_ip                += p.a_pc * (1-p.f_op) * phyto_death
    S_op                += p.a_pc * p.f_op * phyto_death


    # mineralization
    mineralization_p     = p.k_m2 * (p.temp_m2**(T_w-20)) * C_op #* C_p / (p.K_mPc+C_p)

    S_ip                 += mineralization_p
    S_op                 -= mineralization_p

    # enzyme synthesis
    synthesis_em        = p.k_E * p.KI_E / (p.KI_E + C_ip)
    S_p                 += -synthesis_em * C_p
    S_op                += p.a_pc * synthesis_em * C_p
    S_em                += synthesis_em * C_p - p.d_E * C_em

    # enzyme mineralization
    V_min               = p.V_max * C_em * C_op * p.KI_E / ((p.K_OP + C_op) * (p.KI_E + C_ip))
    S_ip                += V_min
    S_op                -= V_min

    #print(phyto_growth - phyto_death - synthesis_em)
    return [S_p,S_ip,S_op,S_em]
