#!/usr/bin/python3.5
#-*- coding: utf-8 -*-

'''
###############
# Modèle WASP #
###############
Dernière mise à jour: 05-07-2016.\n
Réalisé par Julien Diot, julien.diot@supagro.fr\n
Dans le cadre d'un stage ingénieur agronome 2eme année (Montpellier SupAgro).\n
Ce module comprend toutes les paramètres utilisées par le model WASP.\n
----'''

day2seg = 24*3600
## Définition des paramètres du modèle:
k_d1= 0.2/day2seg  	#CBOD deoxygenation rate (s-1)
k_r2= 0.1/day2seg 	#Phytoplankton respiration rate (s-1)
k_r3= 0/day2seg 	#Bacterial respiration rate (mg_O2/d)
k_ni= 0.2/day2seg 	#Nitrification rate (s-1)
k_dn= 0.11/day2seg 	#Denitrification rate (s-1)
k_gr= 1.28/day2seg  #Phytoplankton optimum growth rate (s-1)
k_par=0.02/day2seg  # k_par+k_grz = Phytoplankton basal loss rate (s-1)
k_grz=0.02/day2seg  # k_par+k_grz = Phytoplankton basal loss rate (s-1)
k_m1= 0.075/day2seg	#Organic nitrogen mineralization rate (s-1)
k_m2= 0.2/day2seg 	#Organic phosphorus mineralization rate (s-1)

temp_r1= 1.028	#Temperature adjustment for phytoplankton respiration rate (unitless)
temp_d1= 1.047	#Temperature adjustment for deoxygenation rate (unitless)
temp_ni= 1.06 	#
temp_r2= 1.05 	#Temperature adjustment for phytoplankton respiration rate (unitless)
temp_dn= 1.06 	#
temp_gr= 1.066	#Temperature adjustment for phytoplankton growth rate (unitless)
temp_mr= 1.02 	#Temperature adjustment for phytoplankton respiration rate (unitless)
temp_m1= 1.06 	#
temp_m2= 1.08 	#
temp_SOD=1.07 	#Temperature adjustment for SOD (unitless)

K_BOD= 0.5 		#Half-saturation concentration for oxygen limitation of CBOD oxidation (mg_O2/l)
K_NITR=0.7 		#Half-saturation concentration for oxygen limitation of nitrification (mg_O2/l)
K_NO3= 0.1 		#Half-saturation concentration for oxygen limitation of denitrification (mg_O2/l)
K_mN=  0.025  	#Half-saturation concentration for nitrogen uptake (mgN/l)
K_mP=  0.001  	#Half-saturation concentration for phosphorus uptake (mgP/l)
K_mPc= 1.0 		#Half-saturation concentration for phytoplankton limitation (mg_C/l)
#K_excr= .... 	#Half-saturation concentration foroxygen limitation of phytoplankton excretion(mg_O2/l)

#les paramètres w_Xs semblent avoir une grosse influance
w_2s= 0 	#Settling velocity of PHYT (m/s)
w_3s= 0 	#Settling velocity of CBOD (m/s)
w_7s= 0 	#Settling velocity of particulate organic nitrogen (m/s)
w_8s= 400.0 #Settling velocity of particulate organic phosphorus (m/s)

SOD= 100.0 	#Sediment oxygen demand rate (mgO2/m2/d)  ~~Max 550, si non cbod croit sans arret, NO3 négartif

f_D3= 0.5 #Fraction of dissolved CBOD
f_D7= 1.0 #Fraction of dissolved organic nitrogen
f_D8= 1.0 #Fraction of dissolved organic phosphorus
f_on= 0.7 #Fraction of dead and respired phytoplankton recycled to organic nitrogen pool
f_op= 0.5 #Fraction of dead and respired phytoplankton recycled to organic phosphorus pool

a_nc= 0.25  	#Phytoplankton nitrogen-carbon ratio
a_pc= 0.025 	#Phytoplankton phosphorus-carbon ratio
a_oc= 32/12  	#Ratio of oxygen to carbon
a_on= 32/14		#Ratio of dioxygen to nitrogen molar mass
a_cchla= 90  	#Phytoplankton carbon to chlorophyll a ratio
#a_cchla= 40

D=2.0 			#Depth of water column (m)
D_s= D 			#the height of the current model segment [m].
D_bar= D 		#Average segment of depth (m)
D_b= 0.25 		#Depth of benthic layer (m)

I_s= 300.0/day2seg  	#Optimal saturing light intensity (ly/s)
K_e_prim= 1.5 	#non algal light attenuation (m-1)
K_c=0.016 		#the extinction coefficient per unit of chlorophyll a  (m2/mgChla)

rho_w= 1.0  	#density of water (g/cm-3)
v_bar= 0.05 	#average water velocity  (m/s)

B1= 24.0/(D_b*1000)	#Benthic flux (mgN/d)
B2= 24.0/(D_b*1000)	#Benthic flux (mgN/d)
B3= 6.0/(D_b*1000)	#Benthic flux (mgP/d)

phi_max=720
#Kc=0.02
fu=0.083

formulation="smith"  #formulation for G_p calculation "smith" or "toro"
tailleLac="large2"  #small, intermediate, large1, large2.
acchla_variable=0 	# 0 si a_cchla constante 1 si acchla depend du temps, 2 si il depend du temps et de la profondeur
Is_constant=0 		# 1 si I_s contante 0 sinon



# half saturation constant of the Michaelis Menten mineralization function
K_OP = 10
# maximal mineralization rate
V_max = 2/day2seg

# inhibition coefficient of the enzymatic reaction
KI  = 1
KI_E = 1

k_E = 10/day2seg

d_E = 100/day2seg
